package com.unisoma.databox.observer.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class DateUtil {

	/**
	 * The constructor was set to private to avoid instancing creation  
	 */
	private DateUtil(){}
	
	
	public static Timestamp getDateTime(String strDate, String pattern, Locale loc) {
	    Timestamp tm = null;
	    SimpleDateFormat sdf = new SimpleDateFormat(pattern, loc); 
		try {	    
		    Date date = sdf.parse(strDate);
		    tm = new Timestamp(date.getTime());
		} catch (ParseException e) {
			tm = null;
		} 
		return tm;
	}
	
	
	public static Timestamp getChangedDate(Timestamp iniDate, int incType, int number){
	    Calendar c = Calendar.getInstance();
	    c.setTimeInMillis(iniDate.getTime());
	    c.add(incType, number);
	    return new Timestamp(c.getTimeInMillis());
	}
	
	
	/** 
	 * Retorna o timestamp atual no 'formato' GMT (e não no timezone do Brasil)
	 * @return java.util.Timestamp
	 */
	public static Timestamp getNow(){
		Calendar time = Calendar.getInstance();
		time.add(Calendar.MILLISECOND, -time.getTimeZone().getOffset(time.getTimeInMillis()));
		return new Timestamp(time.getTimeInMillis()); 
	}
	
	
	public static String getDateTime(Timestamp dte, String pattern) {
	    SimpleDateFormat formatter = new SimpleDateFormat(pattern);
	    Date date = new Date(dte.getTime());
	    return formatter.format(date);
	}
	
	
	/**
	 * Calculate the number of slots between two dates
	 */	
	public static int getSlotBetweenDates(Date iniDate, Date finalDate, long slotSizeInMillis){
	    int response = 0;
	    if (finalDate.after(iniDate)){
	        long diff = (finalDate.getTime() - iniDate.getTime());
	        double val = (diff / slotSizeInMillis);
        	val = Math.ceil(((double)diff / (double)slotSizeInMillis));	
	        response = (int)(val);
	    }
	    return response;
	}
}
