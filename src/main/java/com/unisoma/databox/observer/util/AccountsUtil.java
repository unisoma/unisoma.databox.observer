package com.unisoma.databox.observer.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

public class AccountsUtil {

	private final static Logger logger = Logger.getLogger(AccountsUtil.class.getName());
	
	public static String call(String url, ArrayList<Map<String,String>> headers, String payload, boolean isPost, String accessToken) {
		StringBuffer result = new StringBuffer();
		HttpRequestBase request = null;
		
		HttpClient client = HttpClientBuilder.create().build();
		if (isPost) {
			request = new HttpPost(url);
		} else {
			request = new HttpGet(url);
		}

		if (headers==null) {
			headers = new ArrayList<Map<String,String>>();
			AccountsUtil.push(headers, "authorization", "Bearer " + accessToken);
			AccountsUtil.push(headers, "accept", "application/json");
			AccountsUtil.push(headers, "content-type", "application/json");			
		}
		
		for (Map<String, String> itemHeader : headers) {
	        for (Map.Entry<String, String> item : itemHeader.entrySet()) {
	    		request.addHeader(item.getKey(), item.getValue());
	        }
		}

		HttpResponse response = null;
		BufferedReader rd = null;
		try {
			
			if (isPost && payload!=null && !payload.isEmpty()) {
				StringEntity entity = new StringEntity(payload);
				((HttpPost)request).setEntity(entity);				
			}
			
			response = client.execute(request);
			logger.info("Response [" + url + "] [" + response.getStatusLine().getStatusCode() + "]");
			rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = "";
			while ((line = rd.readLine()) != null) {
			    result.append(line);
			}
		
		} catch (UnsupportedOperationException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result.toString();
	}
	
	public static void push(ArrayList<Map<String, String>> list, String key, String param) {
		Map<String, String> element = new HashMap<String, String>();
		element.put(key, param);
		list.add(element);
	}
	
	
	public static ArrayList<String[]> execute(String script) throws Exception {
		ArrayList<String[]> response = new ArrayList<String[]>();

	    final Process child = Runtime.getRuntime().exec(script);
        
        final BufferedReader stdInput = new BufferedReader(new InputStreamReader(child.getInputStream()));
        String s = null;
        while ((s = stdInput.readLine()) != null) {
    		String[] line = s.split("\\|");
    		response.add(line);
    		logger.info(s);
        }

        final BufferedReader stdError = new BufferedReader(new InputStreamReader(child.getErrorStream()));
        while ((s = stdError.readLine()) != null) {
        	logger.severe(s);
        }

        final int code = child.waitFor();
        if (code != 0) {
            throw new Exception("The CLI command failed [" + code + "]");
        }
        
        return response;
    }
}
