package com.unisoma.databox.observer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.unisoma.databox.observer.agent.GeneralTimer;
import com.unisoma.databox.observer.agent.SystemProperties;
import com.unisoma.databox.util.EnvironmentUtil;

public class Application {
	
	final static Logger logger = Logger.getLogger(Application.class.getName());
	
	
	public static void main(String[] args) {
		logger.info("Iniciando DataBOX :: Observer");
				
		String[] keys = new String[] {SystemProperties.OBSERVER_DBX_SAVE_METRIC_URL, 
						SystemProperties.OBSERVER_DBX_GET_DATA_CATALOG_URL, 
						SystemProperties.OBSERVER_DBX_AUTH_ARGS, 
						SystemProperties.OBSERVER_DBX_AUTH_TOKEN, 
						SystemProperties.OBSERVER_DBX_AUTH_URL, 
						SystemProperties.OBSERVER_JOB_JAR_PATH,
						SystemProperties.OBSERVER_DBX_GET_ALL_DEFAULT_PATH,
						SystemProperties.OBSERVER_DBX_GET_EXPECTATIONS_URL,
						SystemProperties.TRINO_URL, SystemProperties.TRINO_USER, 
						SystemProperties.TRINO_PWD, SystemProperties.TRINO_SQL,
						SystemProperties.OBSERVER_TMP_PATH,
						SystemProperties.OBSERVER_DBX_SAVE_PROFILING_URL}; 
		
		//read properties file (default options)
		SystemProperties prop = EnvironmentUtil.getProperties(keys);
		if (prop!=null) {
			EnvironmentUtil.fetchEnvironment(keys, prop);
			
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(prop.getPropertyValue(SystemProperties.OBSERVER_TMP_PATH) + "logging.properties");
				LogManager.getLogManager().readConfiguration(fis);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			GeneralTimer.getInstance(prop);
		} else {
			logger.warning("DataBOX :: Observer não pode ser iniciado. Arquivo de configuracao não encontrado");
		}
	}
	

}

