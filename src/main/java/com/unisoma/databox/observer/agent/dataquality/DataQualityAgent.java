package com.unisoma.databox.observer.agent.dataquality;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Logger;

import org.json.JSONArray;

import com.unisoma.databox.job.JobObserver;
import com.unisoma.databox.observer.agent.ObserverAgent;
import com.unisoma.databox.observer.agent.SystemProperties;
import com.unisoma.databox.observer.util.AccountsUtil;

public class DataQualityAgent extends ObserverAgent {

	private final static Logger logger = Logger.getLogger(DataQualityAgent.class.getName());

	public void run(String accessToken, SystemProperties properties) {
		
		//obtem a lista as entidades que estao no catalogo
		JSONArray list = super.getDataCatalog(accessToken, properties);

		//verifica se existe alguma entidade que precisa ser coletada neste momento
		Object ids[] = null;
		if (list!=null) {
			logger.info("Catalog size [" + list.length() + "]");
			ids = super.getCatalogToExtract(list, "frequencyQuality", "lastExecutionQuality", false);
		} else {
			logger.info("The catalog is empty...");
		}

		//cria um agente de extracao de meta dado via linha de comando
		if (ids!=null) {
			logger.info("the data quality of catalog [" + ids[0] + "] must be executed!");
			
			//from specific catalog_id, fetch all expectation_catalog...
			JSONArray expectations = this.getCatalogExpectation(ids[0]+"", accessToken, properties);
			
			//get the current destination path for file download...
			String destination = super.getDefaultPath(accessToken, properties);
			
			String queryEncoded = "";
			String payloadEncoded = "";
			try {
				queryEncoded = URLEncoder.encode(ids[2]+"", "UTF-8");
				payloadEncoded = URLEncoder.encode(expectations.toString(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.severe(e.getMessage());
			}

			//chamada via linha de comando passando o id do catalogo por parametro
			try {
				AccountsUtil.execute("java -jar -Xms512m -Xmx2048m " + 
						properties.getPropertyValue(SystemProperties.OBSERVER_JOB_JAR_PATH) + 
						" " + JobObserver.JOB_TYPE_QUALITY + " " + ids[0] + " " + ids[1] + " " + 
						queryEncoded + " " + accessToken + " \"" + destination+ "\" \"" + payloadEncoded + "\"");
			} catch (Exception e) {
				logger.severe(e.getMessage());
			}
			
		} else {
			logger.warning("The quality cannot be executed, check if there is wrong parameters at [dbx_source_engine] table.");		
		}

	}
	
	private JSONArray getCatalogExpectation(String catalogId, String accessToken, SystemProperties properties) {
		String url = properties.getPropertyValue(SystemProperties.OBSERVER_DBX_GET_EXPECTATIONS_URL) + catalogId;
		String response = AccountsUtil.call(url, null, null, true, accessToken);
		if (response!=null && !response.trim().equals("")) {
			return new JSONArray(response);	
		} else {
			return null;
		}
	}
		
}
