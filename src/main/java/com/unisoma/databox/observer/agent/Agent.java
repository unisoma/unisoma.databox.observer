package com.unisoma.databox.observer.agent;

import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONObject;

import com.unisoma.databox.observer.agent.container.ContainerAgent;
import com.unisoma.databox.observer.agent.dataquality.DataQualityAgent;
import com.unisoma.databox.observer.agent.extractor.ExtractorAgent;
import com.unisoma.databox.observer.agent.profiling.ProfilingAgent;
import com.unisoma.databox.observer.util.AccountsUtil;

public class Agent {

	private final static Logger logger = Logger.getLogger(Agent.class.getName());
	

	public void onTimer(SystemProperties properties) {
		logger.info("Starting timer...");
		
		// requisita token para autenticacao 
		String accessToken = this.requestAuth(properties);

		logger.info("####### Colecting data from Data Quality...");
		DataQualityAgent dataQualityAgent = new DataQualityAgent();
		dataQualityAgent.run(accessToken, properties);
		
		logger.info("####### Colecting data from Containers...");
		ContainerAgent containerAgent = new ContainerAgent();
		containerAgent.run(accessToken, properties);

		logger.info("####### Extracting metadata from entities...");
		ExtractorAgent extratorAgent = new ExtractorAgent();
		extratorAgent.run(accessToken, properties);

		logger.info("####### Collecting profiling from entities...");
		ProfilingAgent profilingAgent = new ProfilingAgent();
		profilingAgent.run(accessToken, properties);
		
		
		logger.info("End of timer actions...");
	}


	
	private String requestAuth(SystemProperties properties) {
		String accessToken = "";
		
		ArrayList<Map<String,String>> aheaders = new ArrayList<Map<String,String>>();
		AccountsUtil.push(aheaders, "authorization", properties.getPropertyValue(SystemProperties.OBSERVER_DBX_AUTH_TOKEN));
		AccountsUtil.push(aheaders, "content-type", "application/x-www-form-urlencoded");
		String aurl = properties.getPropertyValue(SystemProperties.OBSERVER_DBX_AUTH_URL);
		String authenticatorResponse = AccountsUtil.call(aurl, aheaders, properties.getPropertyValue(SystemProperties.OBSERVER_DBX_AUTH_ARGS), true, null);

		if (authenticatorResponse!=null && !authenticatorResponse.isEmpty()) {
			JSONObject obj = new JSONObject(authenticatorResponse);
			accessToken = obj.getString("access_token");
		} else {
			logger.warning("Authentication endpoint unavailable");			
		}
		
		return accessToken;
	}







	


}
