package com.unisoma.databox.observer.agent.container;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import com.unisoma.databox.observer.agent.ObserverAgent;
import com.unisoma.databox.observer.agent.SystemProperties;
import com.unisoma.databox.observer.util.AccountsUtil;

public class ContainerAgent extends ObserverAgent {

	private final static Logger logger = Logger.getLogger(ContainerAgent.class.getName());
	
	public void run(String accessToken, SystemProperties properties) {
		
		//executa comando ps -a
		List<ContainerStatusTO> clist = this.getAllContainers();
		logger.info("Number of container [" + clist.size() + "]");
	
		// envia conteudo para ser persistido pela API
		if (accessToken!=null && !accessToken.isEmpty()) {
			logger.info("Sending metrics to application.");
			saveMetric(accessToken, clist, properties);
		}
	}
	
	private List<ContainerStatusTO> getAllContainers() {
		List<ContainerStatusTO> response = null;
		JSONArray content = null;

		try {
			//content = this.execute("docker ps -a --format='" + DOCKER_PS_FORMAT + "'");
			content = this.parsing("/opt/data-box/dockerps.sh");

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (content!=null) {
			response = new ArrayList<ContainerStatusTO>();
			for(int i=0 ; i< content.length(); i++) {
				JSONObject line = content.getJSONObject(i);
				
				JSONArray statContent = null;
				JSONObject stat = null;
				try {
					//statContent = this.execute("docker stats " + line.getString("ID") + " --no-stream --format '" + DOCKER_STATS_FORMAT + "'");
					statContent = this.parsing("/opt/data-box/dockerstat.sh " + line.getString("ID"));
					if (statContent != null && statContent.length()>0) {
						stat = statContent.getJSONObject(0);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				ContainerStatusTO csto = new ContainerStatusTO(line, stat);
				response.add(csto);
			}
		}
		
		return response;
	}

	
	private JSONArray parsing(String script) throws Exception {
		JSONArray response = null;
	    final Process child = Runtime.getRuntime().exec(script);
        final BufferedReader stdInput = new BufferedReader(new InputStreamReader(child.getInputStream()));
        final BufferedReader stdError = new BufferedReader(new InputStreamReader(child.getErrorStream()));
        
        String s = null; String content = "";
        while ((s = stdInput.readLine()) != null) {
        	if (!content.equals("")) {
        		content = content.concat(",");
        	}
        	content = content.concat(s);
        }
        response = new JSONArray("[" + content + "]");
        
        while ((s = stdError.readLine()) != null) {
        	logger.severe(s);
        }
        final int code = child.waitFor();
        if (code != 0) {
            throw new Exception("Command on docker failed [" + code + "] [" + script + "] [" + response + "]");
        }
        
        return response;
    }	
	
	private void saveMetric(String accessToken, List<ContainerStatusTO> clist, SystemProperties properties) {
		String url = properties.getPropertyValue(SystemProperties.OBSERVER_DBX_SAVE_METRIC_URL);
		for (ContainerStatusTO metric : clist) {
			String payload = metric.toJson();
			String response = AccountsUtil.call(url, null, payload, true, accessToken);
			logger.info("Saving response.. [" + response + "]");		
		}
	}
	
}
