package com.unisoma.databox.observer.agent.profiling;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.json.JSONArray;

import com.unisoma.databox.job.metadata.MetaDataExtractor;
import com.unisoma.databox.job.metadata.transfer.CatalogDataTO;
import com.unisoma.databox.job.metadata.transfer.ArgumentTO;
import com.unisoma.databox.observer.agent.ObserverAgent;
import com.unisoma.databox.observer.agent.SystemProperties;
import com.unisoma.databox.observer.util.AccountsUtil;
import com.unisoma.databox.observer.util.DateUtil;
import com.unisoma.databox.util.LoggerTrail;

public class ProfilingAgent extends ObserverAgent {

	private final static Logger logger = Logger.getLogger(ProfilingAgent.class.getName());
	
	public void run(String accessToken, SystemProperties properties) {
		Timestamp iniProc = DateUtil.getNow();
		
		//obtem a lista as entidades que estao no catalogo
		JSONArray listCatalog = super.getDataCatalog(accessToken, properties);
		
		//verifica se existe alguma entidade que precisa ser coletada neste momento
		Object ids[] = null;
		if (listCatalog!=null) {
			logger.info("Catalog size [" + listCatalog.length() + "]");
			ids = super.getCatalogToExtract(listCatalog, "frequencyProfiling", "lastExecutionProfiling", true);
		} else {
			logger.info("The catalog is empty...");
		}
		
		//cria um agente de extracao de meta dado via linha de comando
		if (ids!=null) {
			logger.info("the profiling of catalog [" + ids[0] + "] must be executed!");
			
			//get default path to store files...
			String defaultPath = super.getDefaultPath(accessToken, properties) + File.separator + "export" + File.separator;
			
			try {
				String prefix = defaultPath + "PROFILING_META_DATA_" + ids[1];
				File scriptFile = new File(prefix + ".py" );
				File htmlFile = new File(prefix + ".html" );
				File dumpFile = new File(prefix + ".csv" );
				scriptFile.delete();
				htmlFile.delete();
				dumpFile.delete();
				
				//check with extractor/observer must be performed...
				MetaDataExtractor extractor = MetaDataExtractor.builder(null, iniProc.getTime());
				
				ArrayList<ArgumentTO> list = new ArrayList<>();
				ArgumentTO arg1 = new ArgumentTO(SystemProperties.TRINO_URL, properties.getPropertyValue(SystemProperties.TRINO_URL));
				ArgumentTO arg2 = new ArgumentTO(SystemProperties.TRINO_USER, properties.getPropertyValue(SystemProperties.TRINO_USER));
				ArgumentTO arg3 = new ArgumentTO(SystemProperties.TRINO_PWD, properties.getPropertyValue(SystemProperties.TRINO_PWD));
				ArgumentTO arg4 = new ArgumentTO(SystemProperties.TRINO_SQL, ""+ids[2]);
				list.add(arg1); list.add(arg2); list.add(arg3); list.add(arg4);
				
				CatalogDataTO catalogData = extractor.extractData(list, defaultPath + File.separator, dumpFile);
				
				//generate python script ...
				this.writePythonScript(scriptFile, htmlFile, dumpFile);
				
				//perform profiling..
				ArrayList<LoggerTrail> logs = new ArrayList<>();
				this.runPythonScript(logs, scriptFile);
				
				catalogData.setCatalogId(ids[0]+"");
				catalogData.setMetaDataId(ids[1]+"");
				ArrayList<LoggerTrail> previousLogs = catalogData.getLoggerTrails();
				logs.addAll(previousLogs);
				catalogData.setLoggerTrail(logs);
				this.sendProfilingData(accessToken, properties, catalogData.toJson());
				
			} catch (Exception e) {
				logger.severe(e.getMessage());
			}

		}
	}
	
	private void writePythonScript(File scriptFile, File htmlFile, File dumpFile) throws IOException {
		FileWriter w = null;
		
		try {
			
			String dumpFileStr = dumpFile.getAbsolutePath();
			dumpFileStr = dumpFileStr.replaceAll("\\\\", "/");

			String htmlFileStr = htmlFile.getAbsolutePath();
			htmlFileStr = htmlFileStr.replaceAll("\\\\", "/");
		
			w = new FileWriter(scriptFile);
			w.write("import numpy as np\n");
			w.write("import pandas as pd\n");
			w.write("from pandas_profiling import ProfileReport\n");
			w.write("df = pd.read_csv(\"" + dumpFileStr + "\")\n");
			w.write("profile = ProfileReport(df, title=\"Relatorio de Profiling\", explorative=True)\n");
			w.write("profile.to_file(\"" +  htmlFileStr + "\")\n");
		} catch (IOException e) {
			throw e;
		} finally {
			try {
				w.flush();
				w.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	private void runPythonScript(ArrayList<LoggerTrail> logTrails, File pythonFile) throws IOException {
		Process process = null;

		try {
			process = Runtime.getRuntime().exec("python3.8 " + pythonFile.getPath());
		} catch (IOException e) {
			throw e;
		}
		
		InputStream stdout = process.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(stdout, StandardCharsets.UTF_8));
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				logTrails.add(new LoggerTrail(line, "PROFILING"));
			}
		} catch (IOException e) {
			throw e;
		}
	}
	

	
	private void sendProfilingData(String accessToken, SystemProperties properties, String payload) {
		String url = properties.getPropertyValue(SystemProperties.OBSERVER_DBX_SAVE_PROFILING_URL);
		String response = AccountsUtil.call(url, null, payload, true, accessToken);
		logger.info(response);
	}
	
}