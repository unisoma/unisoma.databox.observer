package com.unisoma.databox.observer.agent.extractor;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Logger;

import org.json.JSONArray;

import com.unisoma.databox.job.JobObserver;
import com.unisoma.databox.observer.agent.ObserverAgent;
import com.unisoma.databox.observer.agent.SystemProperties;
import com.unisoma.databox.observer.util.AccountsUtil;

public class ExtractorAgent extends ObserverAgent {

	private final static Logger logger = Logger.getLogger(ExtractorAgent.class.getName());
	
	public void run(String accessToken, SystemProperties properties) {
		
		//obtem a lista as entidades que estao no catalogo
		JSONArray list = super.getDataCatalog(accessToken, properties);
		
		//verifica se existe alguma entidade que precisa ser coletada neste momento
		Object ids[] = null;
		if (list!=null) {
			logger.info("Catalog size [" + list.length() + "]");
			ids = super.getCatalogToExtract(list, "frequency", "lastExecution", false);
		} else {
			logger.info("The catalog is empty...");
		}
		
		//cria um agente de extracao de meta dado via linha de comando
		if (ids!=null) {
			logger.info("the catalog [" + ids[0] + "] must be executed!");
			
			String queryEncoded = "";
			try {
				queryEncoded = URLEncoder.encode(ids[2]+"", "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.severe(e.getMessage());
			}
			
			//chamada via linha de comando passando o id do catalogo por parametro
			try {
				AccountsUtil.execute("java -jar " + 
						properties.getPropertyValue(SystemProperties.OBSERVER_JOB_JAR_PATH) + 
						" " + JobObserver.JOB_TYPE_EXTRACT + " " + ids[0] + " " + ids[1] + 
						" " + queryEncoded + " " + accessToken);
			} catch (Exception e) {
				logger.severe(e.getMessage());
			}

		}
	}
	
}

