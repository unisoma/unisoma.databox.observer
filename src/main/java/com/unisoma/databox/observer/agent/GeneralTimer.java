package com.unisoma.databox.observer.agent;

import java.util.logging.Logger;

public class GeneralTimer implements Runnable {

	final static Logger logger = Logger.getLogger(GeneralTimer.class.getName());
	
	/** Static instance of current singleton */	
	private static GeneralTimer instance = null;
	
	private static long SLEEP_TIME = 180000; //debug time interval (3min)
		
	/** The timer thread */
	private static Thread timer;
	
	/** boolean used by run method to stop the timer process */
	private static boolean stopTimer;

	/** system properties (from default properties file or environment variable */
	private static SystemProperties properties = null;
	
	/**
	 * Constructor
	 */
	private GeneralTimer() {	
	}

	
	/**
	 * This method starts the singleton.
	 */
	public static GeneralTimer getInstance(SystemProperties prop) {
		if (instance == null) {			
			instance = new GeneralTimer();
			properties = prop;
			
			//Create the thread supplying it with the runnable object
			timer = new Thread(GeneralTimer.getInstance(prop));
			timer.setName("DataBOX_Observer_timer");
			timer.setPriority(Thread.MIN_PRIORITY);
			stopTimer = false;
			timer.start();
		}
		return instance;
	}

	
	/** 
	 * This method stops the current timer 
	 * */
	public static void stopTimer(){
		if (timer!=null) {
			stopTimer = false;
		}
	}
	
	
	/* 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
    public void run() {        
		while(!stopTimer) {
			logger.fine("timer call on " + System.currentTimeMillis());
			try {				
			    onTimer();
			} catch (Exception e) {
				logger.severe(e.getMessage());
				e.printStackTrace();
			}
			try {
			    //sleep for the given interval
				Thread.sleep(SLEEP_TIME);
			} catch (InterruptedException es) {
				logger.severe(es.getMessage());
				es.printStackTrace();
			}
		}
	
    }
    
	/**
	 * Function executed when the timer expires
	 * @throws BusinessException
	 */
	protected void onTimer() {
		Agent agent = new Agent();
		agent.onTimer(properties);
	} 
	


}
