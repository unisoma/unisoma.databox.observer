package com.unisoma.databox.observer.agent.container;

import java.text.DecimalFormat;

import org.json.JSONObject;

public class ContainerStatusTO {

	//dados basicos do container
	String containerId;
	String containerImage;
	String createdAt;
	String runningFor;
	String ports;
	String state;
	String status;
	String size;
	String names;
	String mounts;
	String networks;

	//dados estatisticos do container
	String cpuPerc;
	String pids;
	String memPerc;
	String memUsage;
	String memTotal;
	String netUsage;
	String netTotal;
	String blockUsage;
	String blockTotal;
	
	public ContainerStatusTO(String containerId, String containerImage, String createdAt, String runningFor,
			String ports, String state, String status, String size, String names, String mounts,
			String networks) {
		super();
		this.containerId = containerId;
		this.containerImage = containerImage;
		this.createdAt = createdAt;
		this.runningFor = runningFor;
		this.ports = ports;
		this.state = state;
		this.status = status;
		this.size = size;
		this.names = names;
		this.mounts = mounts;
		this.networks = networks;
	}


	public ContainerStatusTO(JSONObject baseData, JSONObject stat) {		
		containerId = this.getVal(baseData, "ID");
		containerImage = this.getVal(baseData, "Image");
		createdAt = this.getVal(baseData, "CreatedAt");
		runningFor = this.getVal(baseData, "RunningFor");
		ports = this.getVal(baseData, "Ports");
		state = this.getVal(baseData, "State");
		status = this.getVal(baseData, "Status");
		size = this.getVal(baseData, "Size");
		names =this.getVal(baseData, "Names");
		mounts = this.getVal(baseData, "Mounts");
		networks = this.getVal(baseData, "Networks");

		cpuPerc  = this.getVal(stat, "CPUPerc").replaceAll("%", "");
		pids = this.getVal(stat, "PIDs");
		memPerc = this.getVal(stat, "MemPerc").replaceAll("%", "");
		
		memUsage = this.toBytes(this.getVal(stat,"MemUsage"), true);
		memTotal = this.toBytes(this.getVal(stat,"MemUsage"), false);

		netUsage = this.toBytes(this.getVal(stat,"NetIO"), true);
		netTotal = this.toBytes(this.getVal(stat,"NetIO"), false);

		blockUsage = this.toBytes(this.getVal(stat,"BlockIO"), true);
		blockTotal = this.toBytes(this.getVal(stat,"BlockIO"), false);
		
	}


	private String getVal(JSONObject element, String key) {
		String response = "";
		try {
			response = element.getString(key);
		} catch(Exception e){
			response = "";
		}
		return response;
	}


	private String toBytes(String content, boolean firstElement) {
		String response = "";
		String[] tokens = content.split("/");
		if (tokens!=null && tokens.length==2) {
			String element = tokens[firstElement?0:1].toLowerCase();
			response = this.checkValue(element, "mb", 1000000); 
			if (response==null) {
				response = this.checkValue(element, "mib", 1048576); 
				if (response==null) {
					response = this.checkValue(element, "gb", 1000000000); 
					if (response==null) {
						response = this.checkValue(element, "gib", 1073741824); 
						if (response==null) {
							response = this.checkValue(element, "kb", 1000); 
							if (response==null) {
								response = this.checkValue(element, "kib", 1024); 
								if (response==null) {
									response = this.checkValue(element, "b", 1); 
								}
							}
						}
					}
				}
			}
		}
		return response;
	}


	private String checkValue(String element, String token, int valueInBytes) {
		String response = null;
		if (element.indexOf(token)>-1) {
			element = element.replaceAll(token, "");
			element = element.trim();
			
			float f = Float.valueOf(element);
			
			DecimalFormat df = new DecimalFormat("##############");
			response = df.format(f * valueInBytes);
		}
		return response;
	}


	public String getContainerId() {
		return containerId;
	}


	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}


	public String getContainerImage() {
		return containerImage;
	}


	public void setContainerImage(String containerImage) {
		this.containerImage = containerImage;
	}


	public String getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}


	public String getRunningFor() {
		return runningFor;
	}


	public void setRunningFor(String runningFor) {
		this.runningFor = runningFor;
	}


	public String getPorts() {
		return ports;
	}


	public void setPorts(String ports) {
		this.ports = ports;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getSize() {
		return size;
	}


	public void setSize(String size) {
		this.size = size;
	}


	public String getNames() {
		return names;
	}


	public void setNames(String names) {
		this.names = names;
	}


	public String getMounts() {
		return mounts;
	}


	public void setMounts(String mounts) {
		this.mounts = mounts;
	}


	public String getNetworks() {
		return networks;
	}


	public void setNetworks(String networks) {
		this.networks = networks;
	}

	
	

	public String getCpuPerc() {
		return cpuPerc;
	}


	public void setCpuPerc(String cpuPerc) {
		this.cpuPerc = cpuPerc;
	}


	public String getPids() {
		return pids;
	}


	public void setPids(String pids) {
		this.pids = pids;
	}


	public String getMemPerc() {
		return memPerc;
	}


	public void setMemPerc(String memPerc) {
		this.memPerc = memPerc;
	}


	public String getMemUsage() {
		return memUsage;
	}


	public void setMemUsage(String memUsage) {
		this.memUsage = memUsage;
	}


	public String getMemTotal() {
		return memTotal;
	}


	public void setMemTotal(String memTotal) {
		this.memTotal = memTotal;
	}


	public String getNetUsage() {
		return netUsage;
	}


	public void setNetUsage(String netUsage) {
		this.netUsage = netUsage;
	}


	public String getNetTotal() {
		return netTotal;
	}


	public void setNetTotal(String netTotal) {
		this.netTotal = netTotal;
	}


	public String getBlockUsage() {
		return blockUsage;
	}


	public void setBlockUsage(String blockUsage) {
		this.blockUsage = blockUsage;
	}


	public String getBlockTotal() {
		return blockTotal;
	}


	public void setBlockTotal(String blockTotal) {
		this.blockTotal = blockTotal;
	}


	public String toJson() {
		return "{ \"containerId\": \"" + containerId + "\", \"containerImage\": \"" + containerImage + "\", \"createdAt\": \"" + createdAt + "\"," +
			  " \"mounts\": \"" + mounts + "\", \"names\": \"" + names + "\"," +
			  " \"networks\": \"" + networks + "\", \"ports\": \"" + ports + "\", \"runningFor\": \"" + runningFor + "\"," +
			  " \"cpuPerc\": \"" + cpuPerc + "\", \"pids\": \"" + pids + "\", \"memPerc\": \"" + memPerc + "\"," +
			  " \"memUsage\": \"" + memUsage + "\", \"memTotal\": \"" + memTotal + "\", \"netUsage\": \"" + netUsage + "\"," +
			  " \"netTotal\": \"" + netTotal + "\", \"blockUsage\": \"" + blockUsage + "\", \"blockTotal\": \"" + blockTotal + "\"," +
			  " \"size\": \"" + size + "\", \"state\": \"" + state + "\", \"status\": \"" + status + "\" }";
	}
	
}
