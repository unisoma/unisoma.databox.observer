package com.unisoma.databox.observer.agent;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import com.unisoma.databox.observer.util.AccountsUtil;
import com.unisoma.databox.observer.util.DateUtil;

public class ObserverAgent {

	//private final static Logger logger = Logger.getLogger(ObserverAgent.class.getName());
	
	protected JSONArray getDataCatalog(String accessToken, SystemProperties properties) {
		String url = properties.getPropertyValue(SystemProperties.OBSERVER_DBX_GET_DATA_CATALOG_URL);
		String response = AccountsUtil.call(url, null, null, false, accessToken);
		return new JSONArray(response);
	}
	
	protected Object[] getCatalogToExtract(JSONArray list, String timingJsonField, 
			String timestampJsonField, boolean onlyPublic) {
		Object[] response = null;
		
		Iterator<Object> it = list.iterator();
		while(it.hasNext()) {
			JSONObject catalog = (JSONObject)it.next();
			JSONObject sourceEngine = (JSONObject)catalog.get("sourceEngine");
			Long sourceEngineId = sourceEngine.getLong("id");
			Long catalogId = catalog.getLong("id");
			String entityQuery = sourceEngine.getString("entityQuery");
			boolean isPublic = sourceEngine.getBoolean("isPublic");
			
			if (!onlyPublic || (onlyPublic && isPublic)) {
				Object frequencyObj = (Integer)sourceEngine.get(timingJsonField);
				if (frequencyObj!=null) {
					Integer frequency = (Integer)frequencyObj;
					if (frequency!=null) {
						
						Object lastExec = sourceEngine.get(timestampJsonField);
						if (JSONObject.NULL.equals(lastExec)) {
							response = new Object[]{catalogId, sourceEngineId, entityQuery.trim()};
							break;
							
						} else {
							String lastExecutionStr = (lastExec+"").replaceAll("T", " ");
							Timestamp lastExecution = DateUtil.getDateTime(lastExecutionStr, "yyyy-MM-dd HH:mm:ss", new Locale("pt", "BR"));
							Timestamp nextExecution = DateUtil.getChangedDate(lastExecution, Calendar.MINUTE, frequency);
							if (DateUtil.getNow().after(nextExecution)) {
								response = new Object[]{catalogId, sourceEngineId, entityQuery.trim()};
								break;						
							}
						}
					}				
				}				
			}
		}

		return response;
	}
	
	
	protected String getDefaultPath(String accessToken, SystemProperties properties) {
		String response = null;
		String url = properties.getPropertyValue(SystemProperties.OBSERVER_DBX_GET_ALL_DEFAULT_PATH);
		String content = AccountsUtil.call(url, null, null, false, accessToken);
		if (content!=null && !content.isEmpty()) {
			JSONArray values = new JSONArray(content);
			if (values!=null && values.length()>0) {
				JSONObject defValObj = (JSONObject)values.get(0);
				response = defValObj.getString("path");
			}
		}
		return response;
	}

}
