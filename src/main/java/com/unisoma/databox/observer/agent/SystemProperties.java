package com.unisoma.databox.observer.agent;

import java.util.HashMap;

public class SystemProperties {

	public final static String OBSERVER_JOB_JAR_PATH = "OBSERVER_JOB_JAR_PATH";
	
	public final static String OBSERVER_DBX_GET_DATA_CATALOG_URL = "OBSERVER_DBX_GET_DATA_CATALOG_URL";
	public final static String OBSERVER_DBX_SAVE_METRIC_URL      = "OBSERVER_DBX_SAVE_METRIC_URL";
	
	public final static String OBSERVER_DBX_AUTH_URL             = "OBSERVER_DBX_AUTH_URL";
	public final static String OBSERVER_DBX_AUTH_ARGS            = "OBSERVER_DBX_AUTH_ARGS";
	public final static String OBSERVER_DBX_AUTH_TOKEN           = "OBSERVER_DBX_AUTH_TOKEN";
	
	public final static String OBSERVER_DBX_SAVE_METADATA_URL    = "OBSERVER_DBX_SAVE_METADATA_URL";
	public final static String OBSERVER_DBX_SAVE_PROFILING_URL   = "OBSERVER_DBX_SAVE_PROFILING_URL";
	public final static String OBSERVER_DBX_SAVE_DATAQUALITY_URL = "OBSERVER_DBX_SAVE_DATAQUALITY_URL";
	
	public final static String OBSERVER_DBX_GET_ALL_DEFAULT_PATH = "OBSERVER_DBX_GET_ALL_DEFAULT_PATH";
	
	public final static String OBSERVER_DBX_GET_EXPECTATIONS_URL = "OBSERVER_DBX_GET_EXPECTATIONS_URL";
	
	public final static String TRINO_URL  = "TRINO_URL";
	public final static String TRINO_USER = "TRINO_USER";
	public final static String TRINO_PWD  = "TRINO_PWD";
	public final static String TRINO_SQL  = "TRINO_SQL";
	
	public final static String OBSERVER_TMP_PATH = "OBSERVER_TMP_PATH";
	
	private HashMap<String, String> props;
	
	public void setProperty(String key, String value) {
		if (props==null) {
			props = new HashMap<>();
		}
		
		props.put(key, value);
	}
	
	
	public String getPropertyValue(String key) {
		String response = "";
		if (props!=null) {
			response = props.get(key);
		} 
		return response;
	}
}
