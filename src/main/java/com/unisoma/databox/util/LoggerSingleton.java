package com.unisoma.databox.util;

import java.util.ArrayList;
import java.util.logging.Logger;

public class LoggerSingleton {

	final static Logger logger = Logger.getLogger(LoggerSingleton.class.getName());
	
	/** Static instance of current singleton */	
	private static LoggerSingleton instance = null;
	
	private static ArrayList<LoggerTrail> trailList = null;
	
	
	/**
	 * Constructor
	 */
	private LoggerSingleton() {	
	}
	

	/**
	 * This method starts the singleton.
		 */
	public static LoggerSingleton getInstance() {
		if (instance == null) {			
			instance = new LoggerSingleton();
			trailList = new ArrayList<>();
		}
		return instance;
	}
	
	
	public static void addTrail(String content, String category){
        LoggerSingleton.getInstance();    
		trailList.add(new LoggerTrail(content, category));
		logger.info(content);
	}


	public static ArrayList<LoggerTrail> dispose() {
		@SuppressWarnings("unchecked")
		ArrayList<LoggerTrail> list = (ArrayList<LoggerTrail>) trailList.clone();
		trailList = new ArrayList<>();
		return list;
	}

}
