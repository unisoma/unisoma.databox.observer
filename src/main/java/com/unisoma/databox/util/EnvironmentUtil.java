package com.unisoma.databox.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

import com.unisoma.databox.observer.agent.SystemProperties;

public class EnvironmentUtil {
	
	final static Logger logger = Logger.getLogger(EnvironmentUtil.class.getName());
	

	public static SystemProperties getProperties(String[] keys) {
		Properties prop = null;
		SystemProperties response = new SystemProperties();
		InputStream inputStream = null;
		
		try {
			prop = new Properties();
			String propFileName = "application.properties";
 
			inputStream = ClassLoader.getSystemResourceAsStream("application.properties");
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file [" + propFileName + "] not found in the classpath");
			}
			
			for (String propKey : keys) {
				response.setProperty(propKey, prop.getProperty(propKey));
			}
			
		} catch (Exception e) {
			logger.severe("Exception: " + e);
		} finally {
			if (inputStream!=null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					logger.severe("Exception: " + e);
				}			
			}
		}
		return response;
	}

	/**
	 * Fetch environment variables and check if exists.
	 * Otherwise still use the default configuration from properties file...
	 * @param keys
	 */
	public static void fetchEnvironment(String[] keys, SystemProperties prop) {
		for (String propKey : keys) {
			String env  = System.getenv(propKey);
			if (env!=null && !env.isEmpty()) {
				prop.setProperty(propKey, env);
			}				
		}
	}
}
