package com.unisoma.databox.util;

import com.unisoma.databox.observer.util.DateUtil;

public class LoggerTrail {

	private String content;
	
	private String eventLog;
	
	private String category;

	
	public LoggerTrail() {
	}
	
	public LoggerTrail(String cnt, String cat) {
		content = cnt;
		category = cat;
		eventLog = DateUtil.getDateTime(DateUtil.getNow() , "yyyy-MM-dd HH:mm:ss");
	}
	
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getEventLog() {
		return eventLog;
	}

	public void setEventLog(String eventLog) {
		this.eventLog = eventLog;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	
}
