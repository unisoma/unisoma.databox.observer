package com.unisoma.databox.job;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.unisoma.databox.job.metadata.MetaDataExtractor;
import com.unisoma.databox.job.metadata.transfer.ArgumentTO;
import com.unisoma.databox.job.metadata.transfer.CatalogDataTO;
import com.unisoma.databox.job.metadata.transfer.QualityDataTO;
import com.unisoma.databox.observer.agent.SystemProperties;
import com.unisoma.databox.observer.util.AccountsUtil;
import com.unisoma.databox.observer.util.DateUtil;
import com.unisoma.databox.util.EnvironmentUtil;

public class JobObserver {

	final static Logger logger = Logger.getLogger(JobObserver.class.getName());
	
	public final static String JOB_TYPE_QUALITY = "QUALITY";
	public final static String JOB_TYPE_EXTRACT = "EXTRACT";
	
	private static String jobType;
	private static String catalogId;
	private static String metaDataId;
	private static String accessToken;
	private static String entityQueryEncoded;
	
	private static String destinationPath;
	private static String expectationsEncoded;
	
	
	public static void main(String[] args) {
		Timestamp iniProc = DateUtil.getNow();
		
		if (args.length == 5 || args.length == 7) {
			jobType = args[0];
			catalogId = args[1];
			metaDataId = args[2];
			entityQueryEncoded = args[3];
			accessToken = args[4];
			if (args.length == 7) {
				destinationPath = args[5];
				expectationsEncoded = args[6];
			}
		} else {			
			logger.severe("The Observer must receive a bunch of arguments and the first must be [QUALITY] or [EXTRACT]. You sent [" + args.length+ "] arguments.");
			System.exit(1);
		}
		
		logger.info("Iniciando JOB [" + jobType + "] :: Observer for catalog [" + catalogId + "]");
		
		//read properties file (default options)
		String[] keys = new String[] {SystemProperties.OBSERVER_DBX_AUTH_ARGS, 
				SystemProperties.OBSERVER_DBX_AUTH_TOKEN, 
				SystemProperties.OBSERVER_DBX_AUTH_URL,
				SystemProperties.OBSERVER_DBX_SAVE_METADATA_URL,
				SystemProperties.OBSERVER_DBX_GET_ALL_DEFAULT_PATH,
				SystemProperties.OBSERVER_DBX_GET_DATA_CATALOG_URL,
				SystemProperties.OBSERVER_DBX_GET_EXPECTATIONS_URL,
				SystemProperties.OBSERVER_DBX_SAVE_DATAQUALITY_URL,
				SystemProperties.OBSERVER_TMP_PATH,
				SystemProperties.TRINO_URL, 
				SystemProperties.TRINO_USER, 
				SystemProperties.TRINO_PWD};		

		SystemProperties prop = EnvironmentUtil.getProperties(keys);
		if (prop!=null) {
			EnvironmentUtil.fetchEnvironment(keys, prop);
			
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(prop.getPropertyValue(SystemProperties.OBSERVER_TMP_PATH) + "logging.properties");
				LogManager.getLogManager().readConfiguration(fis);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			//check with extractor/observer must be performed...
			MetaDataExtractor extractor = MetaDataExtractor.builder(null, iniProc.getTime());
			
			if (extractor!=null) {
				
				String entityQuery = null;
				try {
					entityQuery = URLDecoder.decode(entityQueryEncoded, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				
				ArrayList<ArgumentTO> list = new ArrayList<>();
				ArgumentTO arg1 = new ArgumentTO(SystemProperties.TRINO_URL, prop.getPropertyValue(SystemProperties.TRINO_URL));
				ArgumentTO arg2 = new ArgumentTO(SystemProperties.TRINO_USER, prop.getPropertyValue(SystemProperties.TRINO_USER));
				ArgumentTO arg3 = new ArgumentTO(SystemProperties.TRINO_PWD, prop.getPropertyValue(SystemProperties.TRINO_PWD));
				ArgumentTO arg4 = new ArgumentTO(SystemProperties.TRINO_SQL, entityQuery);
				list.add(arg1); list.add(arg2); list.add(arg3); list.add(arg4);
				 
				if (jobType.equals(JOB_TYPE_EXTRACT)) {
					CatalogDataTO catalogData = extractor.extractInfo(list);
					catalogData.setCatalogId(catalogId);
					catalogData.setMetaDataId(metaDataId);
					sendMetaData(accessToken, prop, catalogData.toJson());					
				} else if (jobType.equals(JOB_TYPE_QUALITY)) {
					QualityDataTO quality = extractor.extractDataQuality(list, catalogId, destinationPath, expectationsEncoded);
					logger.info("=====> " + quality.toJson());
					sendDataQuality(accessToken, prop, quality.toJson());
				}
			}
		
		} else {
			logger.warning("DataBOX :: Job Observer cannot be starterd. The configuration file was not found.");
		}
		
		System.exit(0);
	}

	private static void sendMetaData(String accessToken, SystemProperties properties, String payload) {
		String url = properties.getPropertyValue(SystemProperties.OBSERVER_DBX_SAVE_METADATA_URL);
		String response = AccountsUtil.call(url, null, payload, true, accessToken);
		logger.info("[JobObserver] [" + response + "]");
	}	
	
	private static void sendDataQuality(String accessToken, SystemProperties properties, String payload) {
		logger.info("call endpoint");
		String url = properties.getPropertyValue(SystemProperties.OBSERVER_DBX_SAVE_DATAQUALITY_URL);
		logger.info("call endpoint: " + url + " accesstoken=" + accessToken);
		String response = AccountsUtil.call(url, null, payload, true, accessToken);
		logger.info("[JobObserver] [" + response + "]");
	}
}
