package com.unisoma.databox.job.metadata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.unisoma.databox.job.metadata.transfer.ArgumentTO;
import com.unisoma.databox.job.metadata.transfer.CatalogColumnTO;
import com.unisoma.databox.job.metadata.transfer.CatalogDataTO;
import com.unisoma.databox.job.metadata.transfer.QualityDataTO;
import com.unisoma.databox.observer.agent.SystemProperties;
import com.unisoma.databox.observer.util.AccountsUtil;
import com.unisoma.databox.observer.util.DateUtil;
import com.unisoma.databox.util.LoggerSingleton;

public class JdbcSourceMetaDataObserver extends MetaDataExtractor implements IMetaDataExtractor {

	public static final String JDBC_SOURCE = "JDBC_SOURCE";
 	private static final String TRINO_DRIVER  = "io.trino.jdbc.TrinoDriver";
	
 	@Override
	public CatalogDataTO extractInfo(List<ArgumentTO> list) {
		return extractAll(list, null);
	}
	
	@Override
	public CatalogDataTO extractData(List<ArgumentTO> list, String destPath, File destPathName) {
		return extractAll(list, destPathName);
	}
	
	
	@Override
	public QualityDataTO extractDataQuality(List<ArgumentTO> list, String catalogId, 
			String destinationPath, String expectationsEncoded) {
		QualityDataTO response = new QualityDataTO();
		 
		response.addLoggerTrail("starting extractDataQuality...catalogId:[" + catalogId+ "]");
		String expectationJson = null;
		try {
			expectationJson = URLDecoder.decode(expectationsEncoded, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		if (expectationJson!=null) {
			response.setCatalogId(catalogId);
			response.addLoggerTrail("json collected [" + expectationJson.length() + "] bytes...");
			
			destinationPath = destinationPath.replaceAll("\"", "");
			File directory = new File(destinationPath + "/data_quality/");
			if (!directory.exists()) {
				directory.mkdirs();
			}
			response.addLoggerTrail("the file will be save at [" + directory.getAbsolutePath() + "]");
			
			expectationJson = expectationJson.substring(1, expectationJson.length()-1);
			expectationJson = StringUtils.normalizeSpace(expectationJson);
			File jsonFile = new File(destinationPath + "/data_quality/expectation_" + catalogId + ".json");
			if (jsonFile.exists()) {
				jsonFile.delete();
			}
			this.saveContentToFile(jsonFile, expectationJson);
			response.addLoggerTrail("the file was successfully saved.");

			//- faz a chamada à imagem do great expectation que deverá:
			//   - no arquivo python dentro da imagem do great expectaion deve iterar todas as expectativas do catalogo 
			//     (provenientes do json) e montar as chamadas no padrão do great expectation
			//   - salvar o resultado html no repositorio de arquivos
			// ATENCAO: o docker precisa ser executado em 'detached mode' senao o java trava!!!
			try {
				ArrayList<String[]> executionLog = AccountsUtil.execute("docker run --rm --name=dbx_ge -d -e CATALOG_ID=" + catalogId + " -v " + destinationPath + "/data_quality:/app dbx_ge:2.1");
				response.addLoggerTrail("Script Python successfully executed");
				response.addLoggerTrails(executionLog);
			} catch (Exception e) {
				response.addLoggerTrail(e.getMessage());
			}
			
			//coleta os resultados de cada expectativa (true ou false) para ser enviado ao backend
			response.addLoggerTrail("Generating Great Expectation status file...");
			File responseFile = new File(destinationPath + "/data_quality/export/QUALITY_RESULT_" + catalogId + ".json");
			response.addLoggerTrail("Great Expectation file: [" + responseFile + "]");
			if (responseFile.exists()) {
				JSONArray lista = null;
				try {
					lista = parsing(responseFile);
					response.addLoggerTrail("expectations...[" + lista.length() + "]");
					for (int i = 0; i < lista.length(); ++i) {
					    JSONObject expectResult = lista.getJSONObject(i);
					    Boolean status = expectResult.getBoolean("status");
					    String result = expectResult.getString("result");
					    String expName = expectResult.getString("expectation");
					    Long expCatId = expectResult.getLong("expectation_catalog_id");
					    response.addExpectationResult(new Long(catalogId), expCatId, expName, (status?1:0), result );
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				response.addLoggerTrail("expectations...[" + response.getExpectations() + "]");
			}			
		}
		
		Timestamp endTime = DateUtil.getNow();
		response.addLoggerTrail("JOB_OBSERVER_TIME|EXTRACT_QUALITY|" + (endTime.getTime()- super.getStartProcTime()));
		
		return response;
	}

	public CatalogDataTO extractAll(List<ArgumentTO> list, File dumpFile) {
		Connection c = null;
		CatalogDataTO response = new CatalogDataTO(null, 0);
		String url = this.getValue(SystemProperties.TRINO_URL, list);
		LoggerSingleton.addTrail("Starting info DB extraction: url:[" + url + "]", JDBC_SOURCE);
		String extractType = "EXTRACT_NUM_ROWS";
		
		if (list!=null && !list.isEmpty()) {
			
			//get current arguments from meta data form
	        DriverManagerDataSource dataSource = new DriverManagerDataSource();

	        dataSource.setUrl(url);        
	        dataSource.setDriverClassName(TRINO_DRIVER);
	        dataSource.setUsername( this.getValue(SystemProperties.TRINO_USER, list) );
	        dataSource.setPassword( this.getValue(SystemProperties.TRINO_PWD, list) );
	        LoggerSingleton.addTrail("Driver: [" + TRINO_DRIVER + "]", JDBC_SOURCE);
	        
	        try {
	        	TimeZone timeZone = TimeZone.getTimeZone("UTC");
	        	TimeZone.setDefault(timeZone);
	        	
				c = dataSource.getConnection();
		        LoggerSingleton.addTrail("Connection done.", JDBC_SOURCE);
		        
				ArrayList<CatalogColumnTO> columns = this.getColumns(list, c, url);
		        LoggerSingleton.addTrail("Columns info done.", JDBC_SOURCE);
				
		        Long rows = new Long(0);
		        if (dumpFile!=null) {
		        	extractType = "EXTRACT_DATA";
		        	this.executeSql(list, columns, c, dumpFile);
		        } else {
					rows = this.getNumberRows(list, c);
			        LoggerSingleton.addTrail("Number of rows [" + rows + "]", JDBC_SOURCE);
		        }
		        
				response = new CatalogDataTO(columns, rows);
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					 if (c!=null) {
						 c.close();
					 }				
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		Timestamp endTime = DateUtil.getNow();
		LoggerSingleton.addTrail("JOB_OBSERVER_TIME|" + extractType + "|" + (endTime.getTime()- super.getStartProcTime()), JDBC_SOURCE);

		response.setLoggerTrail(LoggerSingleton.dispose());
        
		return response;
	}
	


	private Long getNumberRows(List<ArgumentTO> list, Connection c) throws SQLException {
		Long rows = null;
		ResultSet rset = null;
		Statement stmt = null;
		
        try {
    		stmt = c.createStatement();
    		String sql = this.getValue(SystemProperties.TRINO_SQL, list);
            LoggerSingleton.addTrail("[getNumberRows] Standard SQL executed.", JDBC_SOURCE);
            
    		rset = stmt.executeQuery("select count(*) as num_rows from (" + sql + ") as sub");
    		if (rset!=null) {
    	        LoggerSingleton.addTrail("[getNumberRows] get value from ResultSet.", JDBC_SOURCE);
    			if (rset.next()) {
        			long r = rset.getLong("num_rows");
        			rows = new Long(r);			
    			}
    		}
    		
        } catch(Exception e) {
			throw e;      	
		} finally {
			try {
				 if (rset!=null) {
					 rset.close();
				 }	
				 if (stmt!=null) {
					 stmt.close();
				 }		
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return rows;
	}
	
	
	private void executeSql(List<ArgumentTO> list, ArrayList<CatalogColumnTO> columns, Connection c, File dumpFile) throws SQLException {
		ResultSet rset = null;
		Statement stmt = null;
		
        try {
    		stmt = c.createStatement();
    		String sql = this.getValue(SystemProperties.TRINO_SQL, list);
            LoggerSingleton.addTrail("[executeSql] Standard SQL executed.", JDBC_SOURCE);
            
    		rset = stmt.executeQuery(sql);
    		if (rset!=null) {
    			
    			String header = this.getColumnHeader(columns) + "\n";
    			saveContentToFile(dumpFile, header);
    			
    			while (rset.next()) {
    				String row = ""; 
    				for (CatalogColumnTO cto : columns) {
    					if(!row.equals("")) {
    						row = row + ",";
    					}
    					
    					if (cto.getColType().equals("DATE")) {
    						row = row + rset.getDate(cto.getName());
    					} else if (cto.getColType().equals("INTEGER")) {
    						row = row + rset.getInt(cto.getName());
    					} else if (cto.getColType().equals("FLOAT")) {
    						row = row + rset.getFloat(cto.getName());
    					} else if (cto.getColType().equals("DOUBLE")) {
    						row = row + rset.getDouble(cto.getName());
    					} else if (cto.getColType().equals("BOOLEAN")) {
    						row = row + rset.getBoolean(cto.getName());
    					} else if (cto.getColType().equals("TIMESTAMP")) {
    						row = row + rset.getTimestamp(cto.getName());
    					} else {
    						row = row + "\"" + this.formatStr(rset.getString(cto.getName())) + "\"";
    					}
					}
        			saveContentToFile(dumpFile, row+ "\n");
    			}
    		}
    		
        } catch(Exception e) {
			throw e;      	
		} finally {
			try {
				 if (rset!=null) {
					 rset.close();
				 }	
				 if (stmt!=null) {
					 stmt.close();
				 }		
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	private String formatStr(String content) {
		if (content!=null) {
			return content.replaceAll("\"", "\"\"");
		} else {
			return "";	
		}
	}

	private void saveContentToFile(File dumpFile, String content) {
	    RandomAccessFile stream = null;
	    ByteBuffer buffer = null;
	    FileChannel channel = null;
		try {
			stream = new RandomAccessFile(dumpFile, "rw");
			stream.seek(stream.length());
		    channel = stream.getChannel();
		    byte[] strBytes = content.getBytes();
	
		    buffer = ByteBuffer.allocate(strBytes.length);
		    buffer.put(strBytes);
		    buffer.flip();
		    channel.write(buffer);
		    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		    try {
				if (stream!=null) {
					stream.close();
				}
				if (channel!=null) {
				    channel.close();				
				}
		    } catch (IOException e) {
				e.printStackTrace();
			}				
		}
	}
	

	private String getColumnHeader(ArrayList<CatalogColumnTO> columns) {
		String header = "";
		for (CatalogColumnTO cto : columns) {
			if (!header.equals("")) {
				header = header + ",";
			}
			header = header + cto.getName();
		}
		return header;
	}
 
	private ArrayList<CatalogColumnTO> getColumns(List<ArgumentTO> list, Connection c, String url)
			throws SQLException {
		ArrayList<CatalogColumnTO> columns = new ArrayList<CatalogColumnTO>();
		ResultSet rset = null;
		Statement stmt = null;
		
        try {
    		stmt = c.createStatement();
    		
    		String sql = this.getValue(SystemProperties.TRINO_SQL, list);
    		rset = stmt.executeQuery(this.getLimit(url, sql));
	        LoggerSingleton.addTrail("[getColumns] Standard SQL executed.", JDBC_SOURCE);
	        
    		ResultSetMetaData rsMeta = rset.getMetaData();
    		if (rsMeta!=null) {
    	        LoggerSingleton.addTrail("[getColumns] Collecting data from JDBC MetaData.", JDBC_SOURCE);
    			for(int i =1; i<=rsMeta.getColumnCount(); i++) {
    				CatalogColumnTO col = new CatalogColumnTO();
    				col.setName(rsMeta.getColumnLabel(i));
    				col.setColType(this.typeMapping(rsMeta.getColumnType(i)));
        			col.setIsNotNull((rsMeta.isNullable(i)==0?1:0));	    					
    				columns.add(col);
    			}
    		}
    		
        } catch(SQLException e) {
			throw e;      	
		} finally {
			try {
				 if (rset!=null) {
					 rset.close();
				 }	
				 if (stmt!=null) {
					 stmt.close();
				 }		
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return columns;
	}
	
	
	private String getLimit(String url, String sql) {
		String response = "";
		response = "select * from (" + sql + ") sub limit 1";
		return response;
	}
	
	private String typeMapping(int type) {
		if (type==Types.BIGINT || type == Types.TINYINT || type == Types.SMALLINT || type == Types.INTEGER) {
			return "INTEGER";
		} else if (type==Types.FLOAT || type == Types.REAL || type == Types.NUMERIC || type == Types.DECIMAL) {
			return "FLOAT";
		} else if (type == Types.DOUBLE) {
			return "DOUBLE";
		} else if (type==Types.BIT || type == Types.CHAR || type == Types.BINARY || type == Types.VARBINARY || type == Types.LONGVARBINARY) {
			return "BOOLEAN";
		} else if (type==Types.VARCHAR || type == Types.LONGVARCHAR) {
			return "VARCHAR";
		} else if (type==Types.DATE) {
			return "DATE";
		} else if (type==Types.TIME || type == Types.TIMESTAMP) {
			return "TIMESTAMP";
		} else {
			return "";
		}
	}
	
	private JSONArray parsing(File responseFile) throws Exception {
		JSONArray response = null;
		final FileInputStream is = new FileInputStream(responseFile);
        JSONTokener tokener = new JSONTokener(is);
        response = new JSONArray(tokener);
        return response;
    }	
	
}
