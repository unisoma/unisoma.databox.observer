package com.unisoma.databox.job.metadata.transfer;

public enum ColumnTypeEnum {
	VARCHAR(12), INTEGER(4), FLOAT(6), DATE(91);

	private final int valor;
	
	ColumnTypeEnum(int val){
		valor = val;
	}
	
	public int getValor(){
		return valor;
	}
}

