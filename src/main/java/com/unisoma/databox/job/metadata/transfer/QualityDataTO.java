package com.unisoma.databox.job.metadata.transfer;

import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.unisoma.databox.util.LoggerTrail; 

public class QualityDataTO {
	
	private ArrayList<CatalogExpectationTO> expectations;
	
	private ArrayList<LoggerTrail> loggerTrails;
	
	private String catalogId;
	
	public String toJson() {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = "";
		try {
			json = ow.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	
	public ArrayList<CatalogExpectationTO> getExpectations() {
		return expectations;
	}
	public void setExpectations(ArrayList<CatalogExpectationTO> newValue) {
		this.expectations = newValue;
	}



	public void setLoggerTrail(ArrayList<LoggerTrail> newValue) {
		loggerTrails = newValue;
	}
	public ArrayList<LoggerTrail> getLoggerTrails() {
		return loggerTrails;
	}
	public void addLoggerTrails(ArrayList<String[]> trails) {
		for (String[] log : trails) {
			for (int i = 0; i < log.length; i++) {
				addLoggerTrail(log[i]);		
			}
		}
	}
	
	public void addLoggerTrail(String trails) {
		LoggerTrail lt = new LoggerTrail(trails, "DATA_QUALITY");
		if (loggerTrails==null) {
			loggerTrails = new ArrayList<LoggerTrail>();
		}
		loggerTrails.add(lt);
	}	

	public String getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(String newValue) {
		this.catalogId = newValue;
	}


	public void addExpectationResult(Long catalogId, Long expectCatalogId, String expectation, Integer success, String result) {
		if (expectations==null) {
			expectations = new ArrayList<CatalogExpectationTO>();
		}
		CatalogExpectationTO ceto = new CatalogExpectationTO();
		ceto.setExpectation(expectation);
		ceto.setResult(result);
		ceto.setExpectationCatalogId(expectCatalogId);
		ceto.setSuccess(success);
		ceto.setCatalogId(catalogId);
		expectations.add(ceto);
	}


}
