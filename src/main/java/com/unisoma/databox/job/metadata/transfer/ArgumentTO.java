package com.unisoma.databox.job.metadata.transfer;

import org.json.JSONObject;

public class ArgumentTO {

	private String argName;
	
	private String argValue;
	
	public ArgumentTO(JSONObject arg) {
		argName = arg.get("argName")+"";
		argValue = arg.get("argValue")+"";
	}

	public ArgumentTO(String name, String value) {
		argName = name;
		argValue = value;
	}
	
	public String getArgName() {
		return argName;
	}

	public String getArgValue() {
		return argValue;
	}

}
