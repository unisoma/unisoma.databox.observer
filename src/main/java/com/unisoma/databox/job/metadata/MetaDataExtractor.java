package com.unisoma.databox.job.metadata;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import com.unisoma.databox.job.metadata.transfer.ArgumentTO;
import com.unisoma.databox.job.metadata.transfer.CatalogDataTO;
import com.unisoma.databox.job.metadata.transfer.QualityDataTO;


public class MetaDataExtractor  implements IMetaDataExtractor {

	public final static String EXTRACTOR_TEMP_FOLDER = "EXTRACTOR_TEMP_FOLDER";
	
	private final static Logger logger = Logger.getLogger(MetaDataExtractor.class.getName());
 	
	private Long startProcTime;
	
	public static MetaDataExtractor builder(String source, Long startingTime) {
		MetaDataExtractor extractor = null;
		if (source==null || source.equals(JdbcSourceMetaDataObserver.JDBC_SOURCE)) {
			extractor = new JdbcSourceMetaDataObserver();			
		} else {
			logger.severe("Job Observer :: unexpected extractor type [" + source + "].");				
		}
		extractor.startProcTime = startingTime;
		return extractor;
	}

	
	@Override
	public CatalogDataTO extractInfo(List<ArgumentTO> listArgs) {
		throw new RuntimeException("A classe meta data precisa implementar este metodo");
	}
	
	@Override
	public CatalogDataTO extractData(List<ArgumentTO> listArgs, String destPath, File destPathName) {
		throw new RuntimeException("A classe meta data precisa implementar este metodo");
	}
	
	@Override
	public QualityDataTO extractDataQuality(List<ArgumentTO> listArgs, String catalogId, String destinationPath, String expectationsEncoded) {
		throw new RuntimeException("A classe meta data precisa implementar este metodo");
	}
		
	public ArrayList<ArgumentTO> parsingMetaData(JSONObject metaDataObj) {
		ArrayList<ArgumentTO> response = null;
		
		JSONArray map = metaDataObj.getJSONArray("valoresParametrosMap");
		if (map!=null) {
			response = new ArrayList<ArgumentTO>();
			for (Object arg : map) {
				ArgumentTO sea = new ArgumentTO((JSONObject)arg);
				response.add(sea);
			}
		}
		
		return response;
	}

	
	protected String getValue(String key, List<ArgumentTO> list) {
		String response = "";
		for (ArgumentTO arg : list) {
			if (arg.getArgName().equals(key)) {
				response = arg.getArgValue();
				break;
			}
		}
		return response;
	}

	
	protected String getEvnValue(String key) {
		return System.getenv(key);
	}
	
	protected Long getStartProcTime() {
		return startProcTime;
	}
}
