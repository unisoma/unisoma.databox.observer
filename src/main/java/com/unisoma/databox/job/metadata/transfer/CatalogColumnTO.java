package com.unisoma.databox.job.metadata.transfer;

public class CatalogColumnTO {

    private String name;
    
    private String colType;
    
    private Integer isNotNull;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColType() {
		return colType;
	}

	public void setColType(String colType) {
		this.colType = colType;
	}

	public Integer getIsNotNull() {
		return isNotNull;
	}

	public void setIsNotNull(Integer isNotNull) {
		this.isNotNull = isNotNull;
	}

	public String toJson() {
		return "{ \"name\": \"" + name + "\", \"colType\": \"" + colType + "\", \"isNotNull\": \"" + isNotNull + "\" }";
	}
    
    
}
