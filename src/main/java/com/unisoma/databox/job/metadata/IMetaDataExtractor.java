package com.unisoma.databox.job.metadata;

import java.io.File;
import java.util.List;

import com.unisoma.databox.job.metadata.transfer.ArgumentTO;
import com.unisoma.databox.job.metadata.transfer.CatalogDataTO;
import com.unisoma.databox.job.metadata.transfer.QualityDataTO;

public interface IMetaDataExtractor {

	public CatalogDataTO extractInfo(List<ArgumentTO> listArgs);
	
	public CatalogDataTO extractData(List<ArgumentTO> list, String destPath, File destPathName);
	
	public QualityDataTO extractDataQuality(List<ArgumentTO> listArgs, String catalogId, String destinationPath, String expectationsEncoded);
}
