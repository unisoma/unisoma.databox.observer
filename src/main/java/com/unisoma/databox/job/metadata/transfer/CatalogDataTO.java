package com.unisoma.databox.job.metadata.transfer;

import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.unisoma.databox.util.LoggerTrail;

public class CatalogDataTO {
 
	private ArrayList<CatalogColumnTO> catalogColumns;
	
	private ArrayList<LoggerTrail> loggerTrails;
	
	private Long numberRows;

	private String username;
	
	private String catalogId;
	
	private String metaDataId;
	
	public CatalogDataTO() {
	}
	
	
	public CatalogDataTO(int[] types, long rows) {
		ArrayList<CatalogColumnTO> list = new ArrayList<>();
		if (types!=null) {
			for (int t : types) {
				CatalogColumnTO ccto = new CatalogColumnTO();
				if (ColumnTypeEnum.DATE.getValor()==t) {
					ccto.setColType("DATE");
				} else if (ColumnTypeEnum.FLOAT.getValor()==t) {
					ccto.setColType("FLOAT");
				} else if (ColumnTypeEnum.INTEGER.getValor()==t) {
					ccto.setColType("INTEGER");
				} else if (ColumnTypeEnum.VARCHAR.getValor()==t) {
					ccto.setColType("VARCHAR");
				}
				list.add(ccto);
			}			
		}
		catalogColumns = list;
		numberRows = rows;
	}
	
	
	public CatalogDataTO(ArrayList<CatalogColumnTO> columns, Long rows) {
		catalogColumns = columns;
		numberRows = rows;
	}
	
	public ArrayList<CatalogColumnTO> getCatalogColumns() {
		return catalogColumns;
	}

	public Long getNumberRows() {
		return numberRows;
	}
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String toJson() {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = "";
		try {
			json = ow.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return json;
	}
	

	public void setColNames(String[] columns) {
		if (catalogColumns==null) {
			catalogColumns = new ArrayList<CatalogColumnTO>();
		}
		
		int idx = 0;
		for (String colName: columns) {
			CatalogColumnTO ccto = null;
			if (catalogColumns.isEmpty() || catalogColumns.size()<=idx) {
				ccto = new CatalogColumnTO();
				catalogColumns.add(ccto);
			} else {
				ccto = catalogColumns.get(idx);
				idx++;
			}
			ccto.setName(colName);
		}
		
	}


	public void setLoggerTrail(ArrayList<LoggerTrail> trails) {
		loggerTrails = trails;
	}
	public ArrayList<LoggerTrail> getLoggerTrails() {
		return loggerTrails;
	}


	public String getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}


	public String getMetaDataId() {
		return metaDataId;
	}

	public void setMetaDataId(String metaDataId) {
		this.metaDataId = metaDataId;
	}

	
}
