package com.unisoma.databox.job.metadata.transfer;

public class CatalogExpectationValueTO {

	private String argId;
	
	private String argValue;

	private Integer argType;
	
	private Integer argOrder;
	
	private String argDomain;
	
	private String argLabel;
	
	
	public String getArgId() {
		return argId;
	}
	public void setArgId(String newValue) {
		this.argId = newValue;
	}
	

	public String getArgValue() {
		return argValue;
	}
	public void setArgValue(String newValue) {
		this.argValue = newValue;
	}
	
	
	public Integer getArgType() {
		return argType;
	}
	public void setArgType(Integer newValue) {
		this.argType = newValue;
	}
	
	
	public Integer getArgOrder() {
		return argOrder;
	}
	public void setArgOrder(Integer newValue) {
		this.argOrder = newValue;
	}
	
	
	public String getArgDomain() {
		return argDomain;
	}
	public void setArgDomain(String newValue) {
		this.argDomain = newValue;
	}
	
	
	public String getArgLabel() {
		return argLabel;
	}
	public void setArgLabel(String newValue) {
		this.argLabel = newValue;
	}
	
	
}
