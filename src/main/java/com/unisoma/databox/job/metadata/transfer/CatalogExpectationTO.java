package com.unisoma.databox.job.metadata.transfer;

import java.util.ArrayList;
import java.util.List;

public class CatalogExpectationTO {

    private String expectation;
    private List<CatalogExpectationValueTO> expectationValues = null;
    private Integer expectationOrder;
    private Long expectationCatalogId;
    private String expectationDesc;
    private String query;
    private List<TagTO> tags;
    private Long catalogId;
    private String result;    
    private Integer success;
    private Boolean enabled;   
    
	public String getResult() {
		return result;
	}
	public void setResult(String newValue) {
		this.result = newValue;
	}
	
	
	
	public Integer getExpectationOrder() {
		return expectationOrder;
	}
	public void setExpectationOrder(Integer newValue) {
		this.expectationOrder = newValue;
	}
	
	
	public String getExpectation() {
		return expectation;
	}
	public void setExpectation(String newValue) {
		this.expectation = newValue;
	}

	
	
	public String getQuery() {
		return query;
	}
	public void setQuery(String newValue) {
		this.query = newValue;
	}
	
	
	public Long getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(Long newValue) {
		this.catalogId = newValue;
	}
	
	

	public Integer getSuccess() {
		return success;
	}
	public void setSuccess(Integer newValue) {
		this.success = newValue;
	}
	
	

	
	public Long getExpectationCatalogId() {
		return expectationCatalogId;
	}
	public void setExpectationCatalogId(Long newValue) {
		this.expectationCatalogId = newValue;
	}
	
	
	
	public String getExpectationDesc() {
		return expectationDesc;
	}
	public void setExpectationDesc(String newValue) {
		this.expectationDesc = newValue;
	}
	
	
	
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean newValue) {
		this.enabled = newValue;
	}
	
	
	
	public List<TagTO> getTags() {
		return tags;
	}
	public void setTags(List<TagTO> newValue) {
		this.tags = newValue;
	}
	public void addTag(String tagId) {
		if (this.tags==null) {
			this.tags = new ArrayList<TagTO>();
		}
		TagTO tto = new TagTO();
		tto.setTag(tagId);
		tags.add(tto);
	}	
	
	public List<CatalogExpectationValueTO> getExpectationValues() {
		return expectationValues;
	}
	public void setExpectationValues(List<CatalogExpectationValueTO> newValue) {
		this.expectationValues = newValue;
	}
	public void addValue(String argId, String argValue, Integer argType, Integer argOrder, String argDomain, String argLabel) {
		if (expectationValues==null) {
			expectationValues = new ArrayList<CatalogExpectationValueTO>();
		}
		CatalogExpectationValueTO cevto = new CatalogExpectationValueTO();
		cevto.setArgId(argId);
		cevto.setArgValue(argValue);
		cevto.setArgType(argType);
		cevto.setArgOrder(argOrder);
		cevto.setArgDomain(argDomain);
		cevto.setArgLabel(argLabel);
		expectationValues.add(cevto);
	}
  
}
