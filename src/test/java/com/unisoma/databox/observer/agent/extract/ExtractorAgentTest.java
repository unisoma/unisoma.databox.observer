package com.unisoma.databox.observer.agent.extract;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Map;

import org.json.JSONObject;
import org.junit.Test;

import com.unisoma.databox.observer.agent.SystemProperties;
import com.unisoma.databox.observer.agent.profiling.ProfilingAgent;
import com.unisoma.databox.observer.util.AccountsUtil;

public class ExtractorAgentTest {

	@Test 
	public void testMain() { 
		try {   
			String token = requestAuth();
			SystemProperties properties = getProperties();
			ProfilingAgent agent = new ProfilingAgent();
			agent.run(token, properties);
			assertTrue(true);
		} catch (final RuntimeException e) {
			e.printStackTrace();
			assertTrue(true);
		}
	}
	
	
	private SystemProperties getProperties() {
		SystemProperties response = new SystemProperties();
		response.setProperty(SystemProperties.OBSERVER_DBX_GET_DATA_CATALOG_URL, "http://192.168.7.221:9091/databox/api/dbxcatalog/findAll");
		response.setProperty(SystemProperties.OBSERVER_JOB_JAR_PATH, "UNISOMA-job-observer-jar-with-dependencies.jar");
		response.setProperty(SystemProperties.OBSERVER_DBX_GET_ALL_DEFAULT_PATH, "http://192.168.7.221:9091/databox/api/defaultPath/findAll");
		
		response.setProperty(SystemProperties.TRINO_URL, "jdbc:trino://192.168.7.221:8080/postgresql/public_dataset");
		response.setProperty(SystemProperties.TRINO_USER, "unisoma");
		response.setProperty(SystemProperties.TRINO_PWD, "");
		
		return response;
	}


	private String requestAuth() {
		String accessToken = "";
		
		String OBSERVER_DBX_AUTH_URL="http://192.168.7.221:9092/authorization/oauth/token";
		String OBSERVER_DBX_AUTH_ARGS="grant_type=password&username=unisoma&password=unisoma";
		String OBSERVER_DBX_AUTH_TOKEN="Basic SUQtQzIzMjMzQUEtQUI2Ri00REI0LUE2NEQtQjVFRDI0Nzk2NDJBOiRjZDY3OWYyMDczNGM0NzUxOGQ1NTQ1MTgwNjNlMTRkZg==";
			
		ArrayList<Map<String,String>> aheaders = new ArrayList<Map<String,String>>();
		AccountsUtil.push(aheaders, "authorization", OBSERVER_DBX_AUTH_TOKEN);
		AccountsUtil.push(aheaders, "content-type", "application/x-www-form-urlencoded");
		String authenticatorResponse = AccountsUtil.call(OBSERVER_DBX_AUTH_URL, aheaders, OBSERVER_DBX_AUTH_ARGS, true, null);

		if (authenticatorResponse!=null && !authenticatorResponse.isEmpty()) {
			JSONObject obj = new JSONObject(authenticatorResponse);
			accessToken = obj.getString("access_token");	
		}
		
		return accessToken;
	}
	
}
