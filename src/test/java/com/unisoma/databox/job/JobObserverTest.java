package com.unisoma.databox.job;

import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;

import org.json.JSONObject;
import org.junit.Test;

import com.unisoma.databox.observer.util.AccountsUtil;
 
public class JobObserverTest {
   
	/*
	@Test 
	public void testExtractor() { 
		try {
			String token = requestAuth(); 
			String[] args = new String[] {JobObserver.JOB_TYPE_EXTRACT, "21", "21", "bi_prognos_request", token};
			JobObserver.main(args);
			assertTrue(true);
		} catch (final RuntimeException e) {
			e.printStackTrace();
			assertTrue(true);
		} 
	} 
	*/
	  
	@Test 
	public void testQuality() { 
		try {    
		  	String queryEncoded, jsonEncoded = "";
			queryEncoded = URLEncoder.encode("select * from postgresql.public_dataset.bi_prognos_request", "UTF-8");
			jsonEncoded = URLEncoder.encode("\"[{\"expetationValues\":[{\"argId\":\"column\",\"argValue\":\"tempo_minutos\"},{\"argId\":\"result_format\",\"argValue\":\"BASIC\"},{\"argId\":\"min_value\",\"argValue\":\"0\"},{\"argId\":\"max_value\",\"argValue\":\"1000\"}],\"catalogId\":4,\"query\":\"select * from postgresql.public_dataset.bi_aloca_equipe\",\"expectation\":\"expect_column_max_to_be_between\",\"expectationOrder\":1},{\"expetationValues\":[{\"argId\":\"column\",\"argValue\":\"tempo_minutos\"},{\"argId\":\"min_value\",\"argValue\":\"0\"},{\"argId\":\"max_value\",\"argValue\":\"481\"}],\"catalogId\":4,\"query\":\"select * from postgresql.public_dataset.bi_aloca_equipe\",\"expectation\":\"expect_column_mean_to_be_between\",\"expectationOrder\":2}]\"", "UTF-8");
			String token = requestAuth(); 
			String[] args = new String[] {JobObserver.JOB_TYPE_QUALITY, "21", "21", queryEncoded, token, "T:/Repositorios/samples", jsonEncoded};
			JobObserver.main(args);
			assertTrue(true);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			assertTrue(true);
		} catch (final RuntimeException e) {
			e.printStackTrace();
			assertTrue(true);
		}
	}
	
	
	private String requestAuth() {
		String accessToken = "";
		
		String OBSERVER_DBX_AUTH_URL="http://192.168.7.221:9092/authorization/oauth/token";
		String OBSERVER_DBX_AUTH_ARGS="grant_type=password&username=unisoma&password=unisoma";
		String OBSERVER_DBX_AUTH_TOKEN="Basic SUQtQzIzMjMzQUEtQUI2Ri00REI0LUE2NEQtQjVFRDI0Nzk2NDJBOiRjZDY3OWYyMDczNGM0NzUxOGQ1NTQ1MTgwNjNlMTRkZg==";
			
		ArrayList<Map<String,String>> aheaders = new ArrayList<Map<String,String>>();
		AccountsUtil.push(aheaders, "authorization", OBSERVER_DBX_AUTH_TOKEN);
		AccountsUtil.push(aheaders, "content-type", "application/x-www-form-urlencoded");
		String authenticatorResponse = AccountsUtil.call(OBSERVER_DBX_AUTH_URL, aheaders, OBSERVER_DBX_AUTH_ARGS, true, null);

		if (authenticatorResponse!=null && !authenticatorResponse.isEmpty()) {
			JSONObject obj = new JSONObject(authenticatorResponse);
			accessToken = obj.getString("access_token");	
		}
		
		return accessToken;
	}
}
