package com.unisoma.databox.job.metadata;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;

import org.junit.Test;

import com.unisoma.databox.job.metadata.transfer.CatalogDataTO;
import com.unisoma.databox.job.metadata.transfer.ArgumentTO;

public class JdbcSourceMetaDataObserverTest {
 /*
	@Test   
	public void testSqlServer() { 
		try { 
			JdbcSourceMetaDataObserver observer = new JdbcSourceMetaDataObserver();
			ArrayList<SourceEngineArgTO> list = new ArrayList<>();
			SourceEngineArgTO arg1 = new SourceEngineArgTO("JDBC_HOST", "jdbc:jtds:sqlserver://192.168.0.244:1433;DatabaseName=MRFG_SOP");
			SourceEngineArgTO arg2 = new SourceEngineArgTO("JDBC_USER", "sa");
			SourceEngineArgTO arg3 = new SourceEngineArgTO("JDBC_PWD", "amosinU987");
			SourceEngineArgTO arg4 = new SourceEngineArgTO("JDBC_SQL", "select * from scenario");
			list.add(arg1); list.add(arg2); list.add(arg3); list.add(arg4);
   
			CatalogDataTO cdto = observer.extractInfo(list);
			if (cdto!=null && !cdto.getCatalogColumns().isEmpty() && cdto.getNumberRows()==89) {
				assertTrue(true);	
			} else {
				assertTrue(false);
			}
			System.out.println(cdto.toJson());
			
		} catch (final RuntimeException e) {
			assertTrue(true);
		}
	}
	
	@Test   
	public void testPosgres() { 
		try { 
			JdbcSourceMetaDataObserver observer = new JdbcSourceMetaDataObserver();
			ArrayList<SourceEngineArgTO> list = new ArrayList<>();
			SourceEngineArgTO arg1 = new SourceEngineArgTO("JDBC_HOST", "jdbc:postgresql://192.168.0.225:5432/unibi");
			SourceEngineArgTO arg2 = new SourceEngineArgTO("JDBC_USER", "unibi");
			SourceEngineArgTO arg3 = new SourceEngineArgTO("JDBC_PWD", "unibi");
			SourceEngineArgTO arg4 = new SourceEngineArgTO("JDBC_SQL", "select * from public_dataset.bi_aloca_equipe");
			list.add(arg1); list.add(arg2); list.add(arg3); list.add(arg4);
 
			CatalogDataTO cdto = observer.extractInfo(list);
			if (cdto!=null && !cdto.getCatalogColumns().isEmpty() && cdto.getNumberRows()>=329356) {
				assertTrue(true);	
			} else {
				assertTrue(false);
			}
			System.out.println(cdto.toJson());
			
		} catch (final RuntimeException e) {
			assertTrue(true);
		}
	}
	*/
	
	@Test    
	public void testTrino() { 
		try { 
			JdbcSourceMetaDataObserver observer = new JdbcSourceMetaDataObserver();
			ArrayList<ArgumentTO> list = new ArrayList<>();
 
			ArgumentTO arg1 = new ArgumentTO("TRINO_URL", "jdbc:trino://192.168.7.221:8080/");
			ArgumentTO arg2 = new ArgumentTO("TRINO_USER", "AlbertoPereto");
			ArgumentTO arg3 = new ArgumentTO("TRINO_PWD", "");
			ArgumentTO arg4 = new ArgumentTO("TRINO_SQL", "select * from postgresql.public_dataset.bi_projeto");
			list.add(arg1); list.add(arg2); list.add(arg3); list.add(arg4);
   
			File dumpFile = new File("c:\teste.dmp");
			CatalogDataTO cdto = observer.extractAll(list, dumpFile);
			if (cdto!=null && !cdto.getCatalogColumns().isEmpty() && cdto.getNumberRows()>=100) {
				assertTrue(true);	
			} else {
				assertTrue(false);
			}
			System.out.println(cdto.toJson());
			
		} catch (final RuntimeException e) {
			assertTrue(true);
		}
	}
	

}
