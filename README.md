# README #

This README contem dicas de como implementar o modulo Observer. Este módulo é o responsável pela coleta de métricas relativas aos containers Docker e também executar os jobs de coleta de dados das entidades do Catálogo.


### Dicas ###

* Subida do projeto Observer no Nexus

O módulo Observer além de rodar separadamente é também adicionado como uma dependência do projeto principal unisoma-setup (do DataBOX).
Assim, sempre que uma versão mais atualizada for gerada é necessário executar alguns passos adicionais:

   * Antes de executar o build do módulo Observer, abrir o pom.xml e avançar a versão da tag <version>x.x.x</version>
   * Depois que o build for feito, executar a linha abaixo em um prompt dentro da pasta do projeto Observer. Não esqueça de aterar o atributo -Dversion para a mesma versão do arquivo pom.xml alterado anteriormente.
   
   mvn deploy:deploy-file -DgroupId="com.unisoma" -DartifactId=UNISOMA-databox-observer -Dversion="1.0.25" -Dpackaging=jar -Dfile="T:\Repositorios\UNI_DATABOX\unisoma.databox.observer\target\UNISOMA-databox-observer-jar-with-dependencies.jar" -DgeneratePom=false -DrepositoryId=nexus -Durl="http://nexus.unisoma.com.br/repository/maven-releases/"

   * Já no projeto unisoma-setup, editar o pom.xml e procurar pela tag <unisoma.databox.observer> ajustando para a versão apropriada. 
   * Garanta que a versão do Jar do observer realmente apareça na lista de dependencias do projeto principal.
     
     